---
layout: lesson
root: .  # Is the only page that doesn't follow the pattern /:path/index.html
permalink: index.html  # Is the only page that doesn't follow the pattern /:path/index.html
---

<!-- this is an html comment -->

In this lesson module, we will introduce deep learning using neural network
models, which is very powerful in addressing some pressing challenges in
the area of cybersecurity.

The objectives of this deep learning lesson are to help learners to:

 * Understand the needs of large-scale machine learning in cybersecurity.

 * Understand the fundamentals of neural networks and their properties.

 * Gain practical familiarity with neural network model architecture and construction.

 * Gain practical experience with neural network training & inference.

 * Improve (tune) neural network models.

 * Use the computing power of HPC to effectively train, tune, and deploy neural network models.


> ## Prerequisites
>
> * Knowledge and experience of Python programming language.
>   Please refer to DeapSECURE's
>   [list of crash courses on Python][deapsecure-python-courses]
>   for pointers to useful resources to get started with Python programming.
>
> * Basic understanding of data analytics and machine learning.
>   We recommend that learners have taken
>   DeapSECURE's [Big Data][deapsecure-bd-lesson]
>   and [Machine Learning][deapsecure-ml-lesson] lessons
>   to equip themselves with the fundamental knowledge and experiences
>   needed in this lesson on neural networks.
>
{: .prereq}

{% comment %}

Pre-workshop survey link:

[https://odu.co1.qualtrics.com/jfe/form/SV_6VyAsa47MLGMMhD](https://odu.co1.qualtrics.com/jfe/form/SV_6VyAsa47MLGMMhD)
![PRE-survey-qr-wksp-3.png](fig/PRE-survey-qr-wksp-3.png)

{% endcomment %}

Website:
<https://deapsecure.gitlab.io/deapsecure-lesson04-nn>

{% include links.md %}
