---
title: "KERAS: Training Phase"
teaching: 0
exercises: 0
questions:
- "How do we train a KERAS network?"
objectives:
- "First learning objective. (FIXME)"
keypoints:
- "First key point. Brief Answer to questions. (FIXME)"
---
In a previous episode we have defined our network model using the `simple_lstm`
function.
This model is saved to a Python variable called `model`.
Now let us train the model.
Please enter these commands, in this order, to your IPython session:

```python
# Fit model and Cross-Validation, ARCHITECTURE 1 SIMPLE LSTM
epochs = 3
batch_size = 32
model = simple_lstm()
model.fit(X_train, target_train, epochs=epochs, batch_size=batch_size)
loss, accuracy = model.evaluate(X_test, target_test, verbose=1)
print('\nFinal Cross-Validation Accuracy', accuracy, '\n')
print_layers_dims(model)
```

There are two important parameters in the training process:

* An `epoch`, defined as the number of one complete cycle through all the training data,
  and

* the `batch_size`, the number of data points that are processed at once.

The output will look like this:

    Epoch 1/3
    146098/146098 [==============================] - 247s 2ms/step - loss: 0.4913 - acc: 0.7667
    Epoch 2/3
    146098/146098 [==============================] - 247s 2ms/step - loss: 0.4055 - acc: 0.8242
    Epoch 3/3
    146098/146098 [==============================] - 249s 2ms/step - loss: 0.3913 - acc: 0.8309
    48700/48700 [==============================] - 15s 302us/step

    Final Cross-Validation Accuracy 0.8360780287621202

    <keras.engine.input_layer.InputLayer object at 0x7f5e448511d0>
    Input Shape:  (None, 75) Output Shape:  (None, 75)
    <keras.layers.embeddings.Embedding object at 0x7f5e44851550>
    Input Shape:  (None, 75) Output Shape:  (None, 75, 32)
    <keras.layers.recurrent.LSTM object at 0x7f5e44851e48>
    Input Shape:  (None, 75, 32) Output Shape:  (None, 32)
    <keras.layers.core.Dropout object at 0x7f5e44884dd8>
    Input Shape:  (None, 32) Output Shape:  (None, 32)
    <keras.layers.core.Dense object at 0x7f5e44884128>
    Input Shape:  (None, 32) Output Shape:  (None, 1)

This training process above has three epochs.
At the end of each epoch, the total time taken to complete epoch is printed
(about 250 seconds),
the value of the *loss function* (`loss:`) is printed, and the
*accuracy* (`acc:`) of the prediction.
The accuracy refers to the fraction of *training data* outcomes that
are correctly categorized by the network at that particular instance
in time.
As we see, the accuracy increases as we take more epochs.
Finally, the "cross validation accuracy" is computed using the
cross-validation data which we set aside earlier for this purpose.
At the very end, the layers of the network are printed for our reference.

At the end of the training, the model can be saved to disk for later usage:

```python
model_name = "deeplearning_LSTM"
save_model(model_name + ".json", model_name + ".h5")
```

Loading is just as easy:

```python
model_reloaded = load_model(model_name + ".json", model_name + ".h5")
```

## Inference/prediction

Now let's use the network to predict whether a URL is harmful:

```python
test_url_mal = "naureen.net/etisalat.ae/index2.php"
test_url_benign = "sixt.com/php/reservation?language=en_US"
url = test_url_benign
# Step 1: Convert raw URL string in list of lists where characters that are contained in "printable" are stored encoded as integer 
url_int_tokens = [[printable.index(x) + 1 for x in url if x in printable]]
max_len=75
T = sequence.pad_sequences(url_int_tokens, maxlen=max_len)
target_proba = model.predict(T, batch_size=1)
def print_result(proba):
    if proba > 0.5:
        return "malicious"
    else:
        return "benign"
print("Test URL:", url, "is", print_result(target_proba[0]))
```

Very good!


## More Training

We can do more training starting from the model that we had optimized earlier using 3 epochs.

    >>> model.fit(X_train, target_train, epochs=epochs, batch_size=batch_size)
    Epoch 1/3
    146098/146098 [==============================] - 225s 2ms/step - loss: 0.3812 - acc: 0.8372
    Epoch 2/3
    146098/146098 [==============================] - 224s 2ms/step - loss: 0.3731 - acc: 0.8418
    Epoch 3/3
    146098/146098 [==============================] - 224s 2ms/step - loss: 0.3633 - acc: 0.8475
    <keras.callbacks.History at 0x7f5e4960c518>

    # You see the accuracy increases, but slowly.

    # Cross validation:
    >>> loss, accuracy = model.evaluate(X_test, target_test, verbose=1)
    48700/48700 [==============================] - 13s 277us/step

    >>> loss
    0.34860248483671546

    >>> accuracy
    0.8520328542241326

A few points:

* As we are training more, the accuracy increases but it will be more
difficult to improve the accuracy.

* At one point, eventually the cross-validation accuracy will drop,
indicating an overfitted model.
We should stop just before that point.


> ## Effect of batch size
> Try increasing the batch size from 32 to 128 and to a larger value.
>
> * What happens to the accuracy?
> * What happens to the training time?
{:.challenge}

{% include links.md %}

