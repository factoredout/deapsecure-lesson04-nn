---
title: "KERAS: Building the Network"
teaching: 0
exercises: 0
questions:
- "How do we build a neural network on KERAS?"
objectives:
- "Understanding how to build a neural network with KERAS."
keypoints:
- "On KERAS, we can easily build the network by defining the layers and connecting them together."
---

Let us revisit the "Drones" problem introduced in
[Module 3](https://deapsecure.gitlab.io/deapsecure-lesson03-ml/).
We want to use that simple problem to build a simple neural network with KERAS.

Although we use KERAS here, we can still reuse the feature matrices and labels
we have defined in Module 3.
For convenience, we will not retype many of the steps.
In the hands-on subdirectory called `Drones`, there is a script called
`drones_nn.py` which contains all the necessary commands.
Let us review these one by one.

The `prepare_data` subroutine contains all the steps necessary to read
the data, extract the feature matrix and labels, then do the train/dev
split:

```python
def prepare_data(data_home, file_name, test_ratio=.2):
    # For simplicity we will save the prepared data to global vars
    global df_drones
    global all_FM, all_L, label_L
    global train_FM, train_L, dev_FM, dev_L

    df_drones = pandas.read_csv(os.path.join(data_home, file_name))

    df_features = df_drones.copy()
    del df_features["class"]
    # Unsplit feature matrix (all_FM) and label vector (all_L)
    all_FM = df_features.astype("float64").values
    all_L, label_L = categorical_to_numerics(df_drones["class"])
    train_FM, dev_FM, train_L, dev_L = train_test_split(all_FM, all_L,
                                                        test_size=test_ratio)

    return train_FM, train_L, dev_FM, dev_L   # notice shuffing of output results
```

Now we define a function which builds a neural network model for us.
The first model will have no hidden layer, but only output layer.
This is exactly the one-neuron case, but implemented in KERAS:

```python
def Model01_dense_no_hidden(inp_len=6):
    """Definition of deep learning model #1: no hidden layer
    """
    # Create an input layer
    main_input = Input(shape=(inp_len,), name='main_input')
    # The dense layer: This is also an output layer (last fully connected layer)
    # on this model.
    output = Dense(1, activation='sigmoid', name='output')(main_input)

    # Compile model and define optimizer
    model = Model(inputs=main_input, outputs=output)
    adam = Adam(lr=1e-4, beta_1=0.9, beta_2=0.999, epsilon=1e-08, decay=0.0)
    model.compile(optimizer=adam, loss='binary_crossentropy', metrics=['accuracy'])
    return model
```

The model also determines which optimizer to use.
In this case, we use Adam optimizer---the detail of which
can be read, for example, in
[this blog article](
    https://machinelearningmastery.com/adam-optimization-algorithm-for-deep-learning/
).
We can now optimize the model according to the data, then yield the
accuracy of the model as cross-validated by the "dev" dataset:

```python
def optimize_model(model, epochs=5, batch_size=32):
    """Runs an optimization ('training') on a neural network model.
    This must be done only after step01, step02, step03 were completed."""
    global train_FM, train_L, dev_FM, dev_L

    print("Optimizing KERAS model: ", model.name)
    print("Evaluation metric names: ", model.metrics_names)
    model_fit_hist = model.fit(train_FM, train_L,
                               epochs=epochs, batch_size=batch_size)

    print("Cross-validating...")
    loss, accuracy = model.evaluate(dev_FM, dev_L, verbose=1)

    print()
    print("Final Cross-Validation Accuracy", accuracy)
    print("Final Cross-Validation Loss    ", loss)
    print()
    print("Model summary:")
    model.summary()
    print_layers_dims(model)
    return model, model_fit_hist, loss, accuracy
```

We call these functions as follows (with the corresponding output):

```python
model01 = Model01_dense_no_hidden()
results = optimize_model(model01)
```

```output
Optimizing KERAS model:  Model
Epoch 1/5
2400/2400 [==============================] - 0s 28us/step - loss: 8.0053 - acc: 0.5033
Epoch 2/5
2400/2400 [==============================] - 0s 28us/step - loss: 8.0053 - acc: 0.5033
Epoch 3/5
2400/2400 [==============================] - 0s 28us/step - loss: 8.0053 - acc: 0.5033
Epoch 4/5
2400/2400 [==============================] - 0s 28us/step - loss: 8.0053 - acc: 0.5033
Epoch 5/5
2400/2400 [==============================] - 0s 28us/step - loss: 8.0053 - acc: 0.5033
Evaluation metric names:  ['loss', 'acc']
600/600 [==============================] - 0s 47us/step

Final Cross-Validation Accuracy 0.4866666666666667
Final Cross-Validation Loss     8.273955688476562
```

Do you see anything strange here?
The accuracy does not come down.
Why?

The accuracy of this module cannot improve after the first epoch.
Further, an accuracy of nearly 50% means the model is not reliable at all.
We do need adjustable parameter to do better.
Here is a model with one hidden layer:

```python
def Model02_dense_1(inp_len=6, hidden_len=64, W_reg=None):
    """Definition of deep Learning model #2: one hidden layer
    """
    # Create an input layer
    main_input = Input(shape=(inp_len,), name='main_input')
    # Hidden layer #1, taking input from `main_input`
    hidden1 = Dense(hidden_len, activation='relu')(main_input)
    # The dense layer: This is also an output layer (last fully connected layer) on this model.
    output = Dense(1, activation='sigmoid', name='output')(hidden1)

    # Compile model and define optimizer
    model = Model(inputs=main_input, outputs=output)
    adam = Adam(lr=1e-4, beta_1=0.9, beta_2=0.999, epsilon=1e-08, decay=0.0)
    model.compile(optimizer=adam, loss='binary_crossentropy', metrics=['accuracy'])
    return model
```

> ## Further Improvement
>
> In the `drones_nn.py` script there is another model (`Model03_dense_2`)
> that has two hidden layers of 64 neurons each.
> We can improve the network's performance further by adding more layer(s),
> and adjust the hidden layer's sizes (number of neurons at each layer).
> Please experiment with these possibilities.
{: .challenge}

{% include links.md %}

