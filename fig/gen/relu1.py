#!/usr/bin/env python
#
# By Wirawan Purwanto
# Use the code & generated figure freely under CC-BY 4.0

from matplotlib import pyplot
from matplotlib import rc
import numpy as np

relu = lambda z : np.array([ (z1 if z1 > 0 else 0) for z1 in z ])

xvals = np.arange(-5, 5, 0.01)
yvals = relu(xvals)

rc('xtick', labelsize=18)
rc('ytick', labelsize=18)

pyplot.figure(figsize=(8.0, 6.0), dpi=65)
pyplot.plot(xvals, yvals, "-", linewidth=4.0)
pyplot.xlim(-5, 5)
pyplot.ylim(0, 7.5)
pyplot.grid(True)
pyplot.axhline(y=0, color="black") # draw x axis @ y=0
pyplot.axvline(x=0, color="black") # draw y axis @ x=0
pyplot.savefig("relu1.png")
