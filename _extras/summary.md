---
title: Summary
---

Here are our goals of this training:

 * Understanding a need of large-scale machine learning in cybersecurity.

 * Understanding the fundamentals of neural networks and their scaling.

 * Familiarity with neural network construction.

 * Practical experience with neural network training & inference (using KERAS).

{% comment %}

Post-workshop survey link:

[https://odu.co1.qualtrics.com/jfe/form/SV_37rLUTCjMWQjmLz](https://odu.co1.qualtrics.com/jfe/form/SV_37rLUTCjMWQjmLz)
![POST-survey-qr-wksp-3.png](fig/POST-survey-qr-wksp-3.png)

{% endcomment %}


{% comment %}
{% include carpentries.html %}
{% endcomment %}
{% include links.md %}
